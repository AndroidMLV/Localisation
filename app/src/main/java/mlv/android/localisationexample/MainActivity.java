package mlv.android.localisationexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button showLocaleButton = (Button) findViewById(R.id.showLocaleButton);
        showLocaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String locale = getResources().getConfiguration().locale.getDisplayName();
                Toast.makeText(getBaseContext(), locale, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
